// array é um objeto, trabalha de uma forma indexada
// estrutura heterogênea - pode ter string, obj, etc, dentro
// boa prática -> trabalhar com dados homogêneos
console.log(typeof Array, typeof new Array, typeof [])
let aprovados = ['Bia', 'Carlos', 'Ana']
console.log(aprovados)

aprovados = ['Bia', 'Carlos', 'Ana']
console.log(aprovados[0])
console.log(aprovados[1])
console.log(aprovados[2])
console.log(aprovados[3])

aprovados[3] = 'Paulo'
aprovados.push('Abia')
console.log(aprovados.length)

aprovados[9] = 'Rafael'
console.log(aprovados.length)
console.log(aprovados[8] === undefined)

console.log(aprovados)
aprovados.sort()
console.log(aprovados)

delete aprovados[1]
console.log(aprovados[1])
console.log(aprovados[2])

// remove sem deixar undefined
aprovados = ['Bia', 'Carlos', 'Ana']
aprovados.splice(1, 2)
console.log(aprovados)

// remove e adiciona
aprovados = ['Bia', 'Carlos', 'Ana']
aprovados.splice(1, 2, 'Elemento 1', 'Elemento 2')
console.log(aprovados)

// adiciona
aprovados = ['Bia', 'Carlos', 'Ana']
aprovados.splice(1, 0, 'Elemento 1', 'Elemento 2')
console.log(aprovados)