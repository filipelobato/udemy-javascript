// Factory simples
function criarPessoa() {
    return {
        nome: 'Ana',
        sobrenome: 'Silva'
    }
}

const pessoa = {
    nome: 'Ana',
    sobrenome: 'Silva'
}
console.log(criarPessoa() === criarPessoa())
console.log(pessoa === pessoa)