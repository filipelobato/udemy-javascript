// Função em JS é First-Class Object (Citizens)
// Higher-order function

// criar de forma literal 
function fun1() {} // sem retorno ela retorna undefined

console.log(fun1())