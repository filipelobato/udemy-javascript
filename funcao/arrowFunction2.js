// O this não varia dentro de uma Arrow Function, que nem acontece com uma função normal.
function Pessoa() {
    this.idade = 0

    setInterval(() => {
        this.idade++
        console.log(this.idade)
    }, 1000)
}

new Pessoa

// Teve que usar bind e passar o contexto, pois se não passasse a idade ficaria inacessível
function Pessoa() {
    this.idade = 0

    setInterval(function(){
        this.idade++
        console.log(this.idade)
    }.bind(this), 1000)
}

new Pessoa

// Cria uma variavel e armazena o contexto
function Pessoa() {
    this.idade = 0

    const self = this
    setInterval(function(){
        self.idade++
        console.log(self.idade)
    }/*.bind(this)*/, 1000)
}

new Pessoa
