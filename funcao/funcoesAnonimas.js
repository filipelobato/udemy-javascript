// Funcoes anonimas
const soma = function (x, y) {
    return x + y
}

const subtracao = function (x, y) {
    return x - y
}

const imprimirResultado = function (a, b, operacao = soma) {
    console.log(operacao(a, b))
}

imprimirResultado(3, 4, soma)
imprimirResultado(4, 3, subtracao)
imprimirResultado(4, 3, function (x, y) {
    return x - y
})

// Função anonima arrow
imprimirResultado(3, 4, (x, y) => x * y)

const pessoa = {
    falar: function() {
        console.log('Opa')
    }
}