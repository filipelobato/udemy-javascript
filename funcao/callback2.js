const notas = [7.7, 6.5, 1.2, 2.3, 2.1, 3.5, 8.9]
/*
function maiorQueSeis(v) {
    if (v > 6) {
        console.log(v)
    }
}
notas.forEach(maiorQueSeis)
*/

// Sem callback
const notasBaixas1 = []
for (let i in notas) {
    if (notas[i] < 7) {
        notasBaixas1.push(notas[i])
    }
}
console.log(notasBaixas1)

// Com callback
const notasBaixas2 = notas.filter(function (nota) {
    return nota < 7
})
console.log(notasBaixas2)

const notasBaixas3 = notas.filter(nota => nota < 7)
console.log(notasBaixas3)