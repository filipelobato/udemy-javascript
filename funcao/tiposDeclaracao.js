console.log(soma(3,4))

// Function Declaration
// Mesmo utilizando antes de declara, funciona; pois o interpretador lê todas as funções, primeiramente.  
function soma(x, y) {
    return x + y
} 

// Não dá pra utilizá-la sem declarar antes.
// console.log(sub(4,1))

// Function Expression
const sub = function(x, y) {
    return x - y
}

// Não dá pra utilizá-la sem declarar antes.
// Named Function Expression
const mult = function mult(x, y) {
    return x * y
}

