let dobro = function (a) {
    return 2 * a
}

dobro = (a) => {
    return 2 * a
}

dobro = a => 2 * a // return implícito
console.log(dobro(Math.PI))

let ola = function () {
    return 'Olá1'
}
console.log(ola())

ola = () => 'Olá2'
console.log(ola())

ola = _ => 'Olá3' // possui um param
console.log(ola())



// Arrow Function não tem arguments 
const somar = (...arguments) => { 
    let soma = 0       
    for (i in arguments) {
        soma += arguments[i]
    }   
    soma = soma.toFixed(2)
    console.log(soma)
    return soma
}
 
let a = [1, 2, 3, 4, 5]
somar(4, 6)

function soma() {
    let soma = 0
    for (i in arguments) {
        soma += arguments[i]
    }    
    soma = soma.toFixed(2)
    console.log(soma)
    return soma
}
soma(1, 2, 3, 4, 5, 6, 7)