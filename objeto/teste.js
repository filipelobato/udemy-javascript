function myFx() {}

const a = new myFx
console.log(myFx)
console.log(myFx.prototype)
console.log(myFx.__proto__)
console.log(myFx.__proto__ === Function.prototype)
console.log(myFx.prototype.__proto__ === Object.prototype)

const obj = {}
obj.__proto__ = {
    myFx
}

console.log(obj)
console.log(obj.__proto__ === Object.prototype)
console.log(obj.__proto__)