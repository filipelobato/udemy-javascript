const a = 1
const b = 2
const c = 3

// #1
const obj1 = { a: a, b: b, c: c}
// nova sintaxe
// #2
const obj2 = { a, b , c}
console.log(obj1, obj2)

// #3
const nomeAttr =  'nota'
const valorAttr = 7.87

const obj3 = {}
obj3[nomeAttr] = valorAttr
console.log(obj3)

// #4
const obj4 = {[nomeAttr]: valorAttr}
console.log(obj4)

// #5
const obj5 = {
    // tradicional
    funcao1: function() {
        // ...
    }, 
    // reduzida (ES2015)
    funcao2() {
        // ..
    }
}
console.log(obj5)

