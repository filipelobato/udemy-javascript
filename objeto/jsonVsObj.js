const obj = { a: 1, b: 2, c: 3, soma() { return a + b + c }  }
// convert obj -> json
console.log(JSON.stringify(obj))

// nao suporta
//console.log(JSON.parse("{ a: 1, b: 2, c: 3 }"))
//console.log(JSON.parse("{ 'a': 1, 'b': 2 , 'c': 3 }"))

console.log(JSON.parse('{ "a": 1, "b": 2, "c": 3 }'))
console.log(JSON.parse('{ "a": 1.7, "b": "string", "c": true, "d": {}, "e": [] }'))
//console.log(JSON.parse('{ "a": 1, "b": \'string\', "c": true, "d": {}, "e": [] }'))
