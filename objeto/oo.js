 // Código não executável !!

 // Procedural - funcoes que manipulavam dados
 const processamento = function (valor1, valor2, valor3) {

 }

 // OO - dados que, dentro deles, têm funções
 objeto = {
     valor1: 1,
     valor2: 2,
     valor3: 3,
     processamento() {
         console.log(this.valor1)
         // ...
     }
 }

 objeto.processamento() // Foco passou a ser o objeto

 /*
Princípios importantes:
  1. Abstração: mapear somente características e comportamentos do mundo real importantes para o software.
  2. Encapsulamento: detalhes da implementação escondidos, quem vai usá-lo não precisa saber tudo.
  3. Herança (prototype): Grau de "parentesco" entre classes. (Ex: Todo carro é um veículo)
  4. Polimorfismo (múltiplas formas): De acordo com a especificidade, o objeto pode ter comportamentos diferente.
*/
