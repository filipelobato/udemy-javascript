const ferrari = {
    modelo: 'F40',
    velMax: 324
}

const volvo = {
    modelo: 'V40',
    velMax: 200
}

const carro = {
    nome: 'carro'
}

console.log(ferrari.__proto__) // acessar o protótipo
console.log(ferrari.__proto__ == Object.prototype) // igual
console.log(ferrari.__proto__ === Object.prototype) // estritamente igual
console.log(Object.prototype.__proto__) // Não há nenhum protótipo do Object.prototype
console.log(Object.prototype.__proto__ == null)

function MeuObjeto() {}
console.log(typeof Object, typeof MeuObjeto)
console.log(Object.prototype)
console.log(MeuObjeto.prototype)
console.log(MeuObjeto.__proto__)
// Objeto não tem prototype, apenas as funções e as funções tem o __proto__