/* Hoisting = jogar pra cima */

console.log('a =', a)
var a = 10
console.log('a =', a)


/* O que acontece internamente
    var a
    console.log('a =', a)
    a = 10
    console.log('a =', a)
*/

function teste() {
    console.log('a =', a)
    var a = 2
    console.log('a =', a)
}


teste()
console.log('a =', a)


console.log('b =', b)
let b = 2
console.log('b =', b)