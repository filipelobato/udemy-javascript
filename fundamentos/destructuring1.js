// novo recurso do ES2015 - Operador destructuring

const pessoa = {
    nome: 'Ana',
    idade: 5,
    endereco: {
        logradouro: 'Rua ABC',
        numero: 1000 
    }
}

// imprimindo normal
console.log(pessoa.endereco.logradouro)

// extraindo da estrutura do objeto
const { nome, idade } = pessoa 
console.log(nome, idade)

// atribuindo variável a atributo estraído da estrutura
const { nome: n, idade: i } = pessoa
console.log(n, i)

// o = true serve para retornar true caso bemHumorada seja undefined
const { sobrenome, bemHumorada = true } = pessoa
console.log(sobrenome, bemHumorada) 

// extrai objeto emdereco dentro de pessoa
const { endereco: { logradouro, numero, cep } } = pessoa
// cep retorna undefined, pois nao existe em pessoa
console.log(logradouro, numero, cep)

// Dá erro, pois não pode desestruturar atributos ag e num de pessoa pq nao existe
const { conta: { ag, num } } = pessoa
console.log(ag, num)