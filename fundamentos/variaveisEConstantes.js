var a = 3 // Var pode ser declarado várias vezes no mesmo escopo
let b = 4 // Recomenda-se usar Let, forma mais moderna

var a = 30
b = 40 // Dá erro caso seja declarado duas vezes no mesmo escopo

console.log(a, b)

a = 300
b = 400

console.log(a, b)

const c = 5 // Só pode ser atribuído o valor uma vez
//c = 50
console.log(c)