var numero = 1
{
    let numero2 = 2
    console.log('dentro =', numero)
}
console.log('fora =', numero)

{
    {
        let a = 13 // não visível fora, principal diferença de var
    }
}

console.log(a)

{
    {
        var b = 12
    }
}
/*
 Var tem escopo de função e global
 Let tem escopo de função, global e de bloco
*/
console.log(b)