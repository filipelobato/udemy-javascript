const escola = "Cod3r"

console.log(escola.substring(0, 2))
console.log(escola.charAt(2))
console.log(escola.charCodeAt(3))
console.log(escola.indexOf(3))
console.log(escola.substr(1))
console.log(escola.substring(0,3))

console.log('Escola: '.concat(escola).concat("!"))
console.log('Escola' +  escola + "!")

console.log(escola.replace(3, 'e'))
console.log(escola.replace(/\d/, 'e')) // regex - substituir todos os dígitos pela letra e
console.log(escola.replace(/\w/g, 'e')) // reges - substitui tudo pela letra e
console.log('Ana,Maria,Pedro'.split(',')) // transforma em array

var nomes = ('Ana,Maria,Pedro'.split(','))
console.log(nomes)