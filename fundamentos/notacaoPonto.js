console.log(Math.ceil(6.1))

const obj1 = {} 
obj1.nome = 'Bola'  // forma de declaração de atributo de objeto similar a de baixo
//obj1['nome'] = 'Bola2'
console.log(obj1.nome)

function Obj(nome) {
    this.nome = nome // se fosse var apenas, ficaria visível somente no escopo da função. o this deixa o nome publico
    this.exec = function() {
        console.log('Exec....')
    }
}


const obj2 = new Obj('Cadeira')
const obj3 = new Obj('Mesa')
console.log(obj2.nome)
console.log(obj3.nome)
obj3.exec()

var myFunction = function() {
    console.log(this);
}

myFunction()
