function tratarErroELancar(erro) {
   // throw new Error('...')
   //throw 10
    //throw true
   throw {
       nome: erro.name,
       msg: erro.message,
       data: new Date
   }
}


function imprimirNomeGritado(obj) {
    try {
        console.log(obj.name.toUpperCase() + '!!!')
    } catch (e) {
        tratarErroELancar(e)
    } finally {
        console.log('Finalizado')
    }    
}

const obj = { nome: 'Roberto' }
imprimirNomeGritado(obj)