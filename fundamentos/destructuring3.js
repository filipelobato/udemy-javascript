// Destructuring

function rand({ min = 0, max = 1000}) {
    const valor = Math.random() * (max - min) + min
    return Math.floor(valor)
}

const obj =  { max: 1500, min: 40, doce: 30}
console.log(rand(obj))
console.log(rand({ min: 955 }))
console.log(rand({}))

// Dá erro, pois ele tenta desestrutura algo que está undefined
console.log(rand()) 