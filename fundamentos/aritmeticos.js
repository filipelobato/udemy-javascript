/* 
    Tipos de sintaxe de operador
    op1 + op2 = INFIX
    ++op = PREFIX
    op++ POSTFIX
*/
/*
    Operador unário: -a
    Operador binário: a + b
    O que opera em apenas 1 operando (unário) e os que operam em dois operando (binário)
*/
const [a, b, c, d] = [3, 5, 1, 15]

const soma = a + b + c + d
const subtracao = d - b
const multiplicacao = a * b
const divisao = d / a
const modulo = a % 2

console.log(soma, subtracao, multiplicacao, divisao, modulo)



