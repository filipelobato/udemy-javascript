// Funcao sem retorno 
function imprimirSoma(a, b) {
    console.log(a + b)
}

function imprimirMultiplicacao(a, b) {
    console.log(a * b)
}

imprimirSoma(4, 2)
imprimirMultiplicacao(4,2)
imprimirSoma(2, 3, 4, 5, 6, 7)
imprimirSoma()

// Funcao com retorno, // padrao 0, introduzido ES 2015
function soma(a, b = 0) { 
    return a + b
}

console.log(soma()) // NaN
console.log(soma(1)) // 1