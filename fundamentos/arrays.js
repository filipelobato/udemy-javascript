const valores = [7.7, 8.9, 9.2, 9.4]

console.log(valores[0], valores[2])
console.log(valores[4]) // undefined

valores[4]= 10
valores[10] = 1 // 5 empty itens
console.log(valores[4])
console.log(valores)
console.log(valores.length)

valores.push({id: 3}, false, null, 'teste')
console.log(valores)

console.log(valores.pop()) // imprime e remove o último valor do array
delete valores[0]
console.log(valores)

console.log(typeof valores)


