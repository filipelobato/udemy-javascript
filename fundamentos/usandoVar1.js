{ 
    { 
        { 
            { 
                var sera = 'Será???'    // var fica visivel nesse bloco, let nao fica visivel             
            } 
        } 
    } 
}

console.log(sera)

function teste() {
    var local = 123
    console.log(local)
}
// Nao enxerga dentro a var dentro da funciton 
teste()
console.log(local)